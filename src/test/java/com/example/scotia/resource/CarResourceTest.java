package com.example.scotia.resource;

import com.example.scotia.ScotiaApplication;
import com.example.scotia.entity.Car;
import com.example.scotia.repository.CarRepository;
import com.example.scotia.service.dto.CarDTO;
import com.example.scotia.service.mapper.CarMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.hamcrest.Matchers.hasItem;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ActiveProfiles("test")
@SpringBootTest(classes = ScotiaApplication.class)
@AutoConfigureMockMvc
class CarResourceTest {

    private static final String TEST_URL = "/api/cars";

    // Default values to save
    private static final String DEFAULT_MODEL = "A";
    private static final Long DEFAULT_YEAR = 1L;

    // Default values to update
    private static final String UPDATED_MODEL = "B";
    private static final Long UPDATED_YEAR = 2L;

    @Autowired private CarRepository carRepository;
    @Autowired private CarMapper carMapper;
    private Car car;
    @Autowired private EntityManager em;

    @Autowired private MockMvc restCar;
    private Validator validator;

    static Car createEntity(){
        return new Car()
                .model(DEFAULT_MODEL)
                .year(DEFAULT_YEAR);
    }

    @BeforeEach
    public void init() {
        car = createEntity();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    /**
     * Create Car record
     *
     * Input: Car model to save
     * Output: Car model with DEFAULT values
     *
     * @throws Exception the exception message
     */
    @Test
    @Transactional
    void createCar() throws Exception {
        int databaseSizeBeforeCreated = carRepository.findAll().size();

        CarDTO carDTO = carMapper.toDto(car);
        restCar.perform(post(TEST_URL)
                .contentType(Testutil.APPLICATION_JSON)
                .content(Testutil.convertObjectToJsonBytes(carDTO)))
                .andExpect(status().isCreated());

        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeCreated + 1);

        Car car = carList.get(carList.size() - 1);
        assertThat(car.getModel()).isEqualTo(DEFAULT_MODEL);
        assertThat(car.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    /**
     * Create Car record should fail
     *
     * @throws Exception the exception message
     */
    @Test
    @Transactional
    void createCarShouldFail() throws Exception {
        int databaseSizeBeforeCreated = carRepository.findAll().size();

        restCar.perform(post(TEST_URL)
                .contentType(Testutil.APPLICATION_JSON)
                .content(Testutil.convertObjectToJsonBytes(null)))
                .andExpect(status().is4xxClientError());

        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeCreated);
    }

    /**
     * Get Car record
     *
     * @throws Exception the exception message
     */
    @Test
    void getCar() throws Exception {
        carRepository.saveAndFlush(car);

        restCar.perform(get(TEST_URL+"/"+car.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(Testutil.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(car.getId().intValue()))
                .andExpect(jsonPath("$.model").value(DEFAULT_MODEL))
                .andExpect(jsonPath("$.year").value(DEFAULT_YEAR));
    }

    /**
     * Check values required
     *
     * Input: Car model with null values
     * Output: boolean that indicate False to violations
     *
     * @throws Exception the exception message
     */
    @Test
    void checkCarRequired() throws Exception {
        car.setModel(null);
        car.setYear(null);
        Set<ConstraintViolation<Car>> violations = validator.validate(car);
        assertFalse(violations.isEmpty());
    }

    /**
     * Get all Car records
     *
     * @throws Exception the exception message
     */
    @Test
    void getAllRecords() throws Exception {
        carRepository.saveAndFlush(car);

        restCar.perform(get(TEST_URL)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(car.getId().intValue())))
                .andExpect(jsonPath("$.[*].model").value(hasItem(car.getModel())))
                .andExpect(jsonPath("$.[*].year").value(hasItem(car.getYear().intValue())));
    }

    /**
     * Update Car record
     *
     * Input: Car model with DEFAULT values
     * Output: Car model with UPDATED values
     *
     * @throws Exception the exception message
     */
    @Test
    @Transactional
    void updateCar() throws Exception {
        carRepository.saveAndFlush(car);

        int databaseSizeBeforeUpdate = carRepository.findAll().size();

        Car carUpdated = carRepository.findById(car.getId()).get();
        // Disconnect from session so that the updates on updatedOrdenTrabajo are not directly saved in db
        em.detach(carUpdated);
        carUpdated.model(UPDATED_MODEL).year(UPDATED_YEAR);

        CarDTO carDTO = carMapper.toDto(carUpdated);
        restCar.perform(put(TEST_URL)
                .contentType(Testutil.APPLICATION_JSON)
                .content(Testutil.convertObjectToJsonBytes(carDTO)))
                .andExpect(status().isOk());

        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeUpdate);
        Car testCar = carList.get(carList.size()-1);
        assertThat(testCar.getId()).isEqualTo(car.getId());
        assertThat(testCar.getModel()).isEqualTo(UPDATED_MODEL);
        assertThat(testCar.getYear()).isEqualTo(UPDATED_YEAR);
    }

    /**
     * Find Car record
     *
     * Input: Car model with Id not present
     * Output: ResponseEntity with code 404 - Not found
     *
     * @throws Exception the exception message
     */
    @Test
    void findCarByIdNotFound() throws Exception {
        carRepository.saveAndFlush(car);

        car.setId(5L);
        restCar.perform(get(TEST_URL+"/"+car.getId())).andExpect(status().is4xxClientError());
    }
}
