package com.example.scotia.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "model", nullable = false)
    private String model;

    @NotNull
    @Column(name = "year", nullable = false)
    private Long year;

    public Long getId() {
        return id;
    }

    public Car setId(Long id) {
        this.id = id;
        return this;
    }

    public String getModel() {
        return model;
    }

    public Car setModel(String model) {
        this.model = model;
        return this;
    }

    public Long getYear() {
        return year;
    }

    public Car setYear(Long year) {
        this.year = year;
        return this;
    }

    public Car id(Long id){
        this.id = id;
        return this;
    }

    public Car model(String model){
        this.model = model;
        return this;
    }

    public Car year(Long year){
        this.year = year;
        return this;
    }

}
