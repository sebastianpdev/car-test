package com.example.scotia.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class CarDTO implements Serializable {

    private Long id;
    @NotNull private String model;
    @NotNull private Long year;

    public Long getId() {
        return id;
    }

    public CarDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getModel() {
        return model;
    }

    public CarDTO setModel(String model) {
        this.model = model;
        return this;
    }

    public Long getYear() {
        return year;
    }

    public CarDTO setYear(Long year) {
        this.year = year;
        return this;
    }
}
