package com.example.scotia.service.mapper;

import com.example.scotia.entity.Car;
import com.example.scotia.service.dto.CarDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CarMapper extends EntityMapper<CarDTO, Car>{

    CarDTO toDto(Car car);
    Car toEntity(CarDTO carDTO);

    default Car fromId(Long id){
        if (id == null) {
            return null;
        }
        Car car = new Car();
        car.setId(id);
        return car;
    }
}
