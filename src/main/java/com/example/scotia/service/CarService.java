package com.example.scotia.service;

import com.example.scotia.entity.Car;
import com.example.scotia.repository.CarRepository;
import com.example.scotia.service.dto.CarDTO;
import com.example.scotia.service.mapper.CarMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public CarService(CarRepository carRepository, CarMapper carMapper){
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public CarDTO save(CarDTO carDTO){
        Car car = carMapper.toEntity(carDTO);
        car = carRepository.save(car);
        return carMapper.toDto(car);
    }

    public Optional<CarDTO> findById(Long id){
        return carRepository.findById(id).map(carMapper::toDto);
    }

    public Page<CarDTO> getAllCars(Pageable page){
        return carRepository.findAll(page).map(carMapper::toDto);
    }

}
