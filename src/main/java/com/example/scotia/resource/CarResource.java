package com.example.scotia.resource;

import com.example.scotia.resource.util.ResponseUtil;
import com.example.scotia.service.CarService;
import com.example.scotia.service.dto.CarDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CarResource {

    private final Logger logger = LoggerFactory.getLogger(CarResource.class);

    private final CarService carService;

    public CarResource(CarService carService){
        this.carService = carService;
    }

    @PostMapping("/cars")
    public ResponseEntity<Long> createCar(@RequestBody CarDTO carDTO) throws URISyntaxException {
        logger.debug("REST request to create car {}", carDTO);
        if (carDTO.getId()!=null){
            throw new RuntimeException("A new car cannot already have an id");
        }
        CarDTO newCar = carService.save(carDTO);
        return ResponseEntity.created(new URI("/api/cars/"+newCar.getId())).body(newCar.getId());
    }

    @GetMapping("/cars/{id}")
    public ResponseEntity<CarDTO> findById(@PathVariable Long id){
        logger.debug("REST request to get car {}", id);
        Optional<CarDTO> carDTO = carService.findById(id);
        return ResponseUtil.wrapOrNotFound(carDTO);
    }

    @GetMapping("/cars")
    public ResponseEntity<List<CarDTO>> getAllCars(Pageable pageable){
        logger.debug("REST request to get all cars");
        Page<CarDTO> page = carService.getAllCars(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", Long.toString(page.getTotalElements()));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PutMapping("/cars")
    public ResponseEntity<CarDTO> updateCar(@Valid @RequestBody CarDTO carDTO) {
        logger.debug("REST request to update car {}", carDTO);
        if (carDTO.getId()==null){
            throw new RuntimeException("Id null, cannot update record");
        }
        CarDTO record = carService.save(carDTO);
        return ResponseEntity.ok().body(record);
    }

}
